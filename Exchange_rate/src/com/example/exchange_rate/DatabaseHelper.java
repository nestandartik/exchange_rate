package com.example.exchange_rate;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper implements BaseColumns{
	
	private static final String DATABASE_NAME = "Exchange_rate.db";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_TABLE = " kyrs ";

    public static final String CCY_COLUMN = " ccy ";
    public static final String BUY_COLUMN = " buy ";
    public static final String SALE_COLUMN = " sale ";
    public static final String CUR_DATE = " cur_date ";
    
    private static final String DATABASE_CREATE_SCRIPT = "create table "
            + DATABASE_TABLE + " ( " + BaseColumns._ID
            + " integer primary key autoincrement, " + CCY_COLUMN
            + " text not null, " + BUY_COLUMN + " double, " + SALE_COLUMN
            + " double, "+ CUR_DATE+" char(50) );";

	public DatabaseHelper(Context exchange_rate, String name, CursorFactory factory,
			int version) {
		super(exchange_rate, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(DATABASE_CREATE_SCRIPT);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
        Log.w("SQLite", "������������ � ���� " + oldVersion + " �� ����� " + newVersion);

        db.execSQL("DROP TABLE IF IT EXISTS " + DATABASE_TABLE);
        onCreate(db);
		
	}
	
	
	DatabaseHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
            int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
	}
}
