package com.example.exchange_rate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.jjoe64.graphview.*;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.LinearLayout;


public class graph extends Fragment {
	private DatabaseHelper mDatabaseHelper;
	 private SQLiteDatabase mSqLiteDatabase; 
	@Override  
     public View onCreateView(LayoutInflater inflater, ViewGroup container,  
               Bundle savedInstanceState) {  
          // TODO Auto-generated method stub  
    	 View view = inflater.inflate(R.layout.graph, container,false);
    	 mDatabaseHelper = new DatabaseHelper(getActivity(), "mydatabase.db", null, 1);
         mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();
         String selection = " ccy = 'USD' ";
         
    	 Cursor cursor = mSqLiteDatabase.query("kyrs",null, selection, null, null, null, null) ;
    	 DataPoint DP1 [] = new DataPoint[] {};
    	 
    	 GraphView graph = new GraphView(getActivity());
    	 graph.setTitle("���������� ����� ����� USD �� ���� ");
    	 //graph.getViewport().setScalable(true);
    	 //graph.getViewport().setScrollable(true);
    	 
 		LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(DP1);
 		double buy_price = 0; 
 		String cur_date;
 		Date date = null;
 		if (cursor != null) {
 		      if (cursor.moveToFirst()) {
 		        do {
 		          for (String cn : cursor.getColumnNames()) {
 		        	  if ((cn).compareToIgnoreCase("buy") == 0){
 		        		  buy_price = cursor.getDouble(cursor.getColumnIndex(cn));
 		        	  }
 		        	 if ((cn).compareToIgnoreCase("cur_date") == 0){
 		        		cur_date =cursor.getString(cursor.getColumnIndex(cn));
 		        		SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd kk:mm:ss");  
 		        		try {  
 		        		    date = format.parse(cur_date);  
 		        		} catch (ParseException e) {  
 		        		    // TODO Auto-generated catch block  
 		        		    e.printStackTrace();  
 		        		}
		        	  }		              
 		          }
 		         series.appendData(new DataPoint(date,buy_price),true,20);
 		        } while (cursor.moveToNext());
 		      }
 		      cursor.close();
 		}
	     graph.addSeries(series);
			LinearLayout layout = (LinearLayout) view.findViewById(R.id.layout111);
			layout.addView(graph);	    
		mSqLiteDatabase.close();
		return view;
	 }
}
